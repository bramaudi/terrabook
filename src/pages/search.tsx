import { Result } from "@/types"
import { createResource } from "solid-js"
import useJson from "@/hooks/useJson"
import ScrollVew from "@/components/ScrollVew"

export default function () {
  const [items] = createResource<string[]>(
    () => fetch('/json/_search.json')
      .then(r => r.json())
      .then((json: Result[]) => json.map(({ name }) => name))
  )
  return (
    <ScrollVew items={items} slug="search" isSearchPage={true} />
  )
}