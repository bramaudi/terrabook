<img src="https://codeberg.org/bramaudi/terrabook/raw/branch/main/site/static/logo.png" alt="Terrabook logo" height="100" />

# Terrabook
> This is still a work in progress to complete all the items

## Introduction

Terrabook is offline handcrafted wiki for Terraria with following features:

* 2600+ items in total
* Simple & focused UI design
* Offline with no internet permission
* Search all or specific items category
* Mark items as favorite
* Free & no ads

More items are coming soon...

### Download

[Download APK](https://codeberg.org/bramaudi/terrabook/releases)
