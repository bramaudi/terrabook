import { basename } from "path";
import Bun from 'bun'

const fetch = type => {
    const child = Bun.spawn(['bun', `./labs/crawler/${type}/run.js`], { stdout: 'pipe' })
    if (child.stdout) {
        new Response(child.stdout).text().then(text => console.log(text));
    }
}

(async () => {
    let [type] = process.argv.slice(2)
    if (type) {
        if (await !Bun.file(`./labs/crawler/${type}/run.js`).exists()) {
            return console.log('type doesn\'t exists');
        }
        fetch(type)
    }
    else {
        const type_dirs = [...new Bun.Glob('./labs/crawler/*').scanSync({ onlyFiles: false })]
            .map(path => basename(path))
            .filter(name => !name.startsWith('_') && !name.includes('.js'))

        for (const dirname of type_dirs) {
            fetch(dirname)
        }
    }
})()