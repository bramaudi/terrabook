import Bun from 'bun'
import { parseHTML } from 'linkedom'

/**
 * Crawler
 * @param {{
 * script: string[]
 * slug: string
 * name: string?
 * update_cache: boolean?
 * verbose: boolean?
 * custom_slug: boolean?
 * }} props 
 * @returns 
 */


/**
 * Crawler
 * @param {string[]} props.script
 * @param {string} props.slug
 * @param {boolean} props.update_cache default: false
 * @param {boolean} props.verbose default: false
 * @param {boolean} props.custom_slug default: false
 */
export default async function crawl(props) {
	let {
		script,
		slug,
		name = null,
		update_cache = false,
		verbose = false,
		custom_slug = false
	} = props
	if (!script) return console.log('"--script" argument cannot be empty')
	if (!slug) return console.log('"--slug" argument cannot be empty')

	const WIKI_URL = 'https://terraria.wiki.gg/' + (custom_slug ? '' : 'wiki/')
	const CACHE = `./labs/crawler/_cache/${custom_slug ? name : slug}.html`
	
	// Create if script folder exists
	const folder = `./public/json`
	if (await !Bun.file(folder).exists()) {
		Bun.$`mkdir ${folder}`
	}
	
	// Get html and save in cache (if update_cache disabled)
	if (await !Bun.file(CACHE).exists() || update_cache) {
		await fetch(WIKI_URL + slug).then(async (res) => {
			if (verbose) console.log(`Caching ${slug}`);
			writeFileSync(CACHE, await res.text())
		})
	}
	
	// Run script and write json result
	let scriptStr = ''
	for (const scriptPath of script) {
		if (await !Bun.file(scriptPath).exists()) return console.log(`--script ${scriptPath} not found`)
		scriptStr += await Bun.file(scriptPath).text()
	}
	const html = await Bun.file(CACHE).text()
	const { document } = parseHTML(html)

	// Saved file name
	name = name || slug
	
	async function json(obj) {
		const target = `${folder}/${name}.json`
		const content = JSON.stringify(obj, null, 2)
		if (verbose) console.log(`Write ${target}`);
		await Bun.write(target, content)
	}

	try {
		new Function('document', 'print', '$url', scriptStr)(document, json, WIKI_URL + slug)
		// process.stdout.write()
		process.stdout.write(`\u001b[32m [✔] ${name}\u001b[39m\n`);
	} catch (error) {
		process.stdout.write(`\u001b[31m [✗] ${name}\u001b[39m\n`);
		await Bun.write(`./labs/crawler/_logs/${name}.txt`, error.stack)
	}

	return
}