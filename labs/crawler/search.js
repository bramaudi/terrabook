import { writeFileSync } from "fs";
import { Glob } from "bun";
import { basename } from "path";

let collection = []

const glob = [...new Glob("./public/json/*.json").scanSync('.')]
    .filter(name => !basename(name).startsWith('_'));

// console.log(glob);
// console.log(glob.find(item => item));

for (const path of glob) {
    const json = await Bun.file(path).json()
    collection.push({
        type: json.type,
        name: basename(path).replace('.json', '')
    })
}

console.log('[Success] Build search data');
const destination = `./public/json/_search.json`
const input = JSON.stringify(collection
    .sort((a, b) => a.name.localeCompare(b.name)), null, 2)

Bun.write(destination, input)