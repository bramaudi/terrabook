#!/bin/bash -ex
DEPLOYMENT_REPO=${1:-git@codeberg.org:bramaudi/terrabook}
DEPLOYMENT_BRANCH=${2:-pages}
DIST=_site
rm -rf $DIST
npx eleventy --config=.eleventy.cjs
( cd $DIST && git init -b "$DEPLOYMENT_BRANCH" )
# echo "example.com" > $DIST/.domains
( cd $DIST && git add -A ) ## add all generated files
( cd $DIST && git commit -m "Deployment at $(date +'%F %T')" ) ## commit all
( cd $DIST && git remote add origin "$DEPLOYMENT_REPO" )
( cd $DIST && git push -f origin "$DEPLOYMENT_BRANCH" ) ## force-push and rewrite (empty) history
