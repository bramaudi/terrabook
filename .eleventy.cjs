module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy("src_pages/static");
    return {
        dir: {
            input: "src_pages"
        },
        pathPrefix: "/terrabook/"
    }
};